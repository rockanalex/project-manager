import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { InicioComponent } from './components/inicio/inicio.component';

const routes: Routes = [
  {
    path : "",
    component : ProjectsComponent
  },
  {
    path : "home",
    component : HomeComponent
  },
  {
    path : "inicio",
    component : InicioComponent
  },
  {
    path : "projects",
    component : ProjectsComponent
  },
  {
    path : "not-found",
    component : NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
