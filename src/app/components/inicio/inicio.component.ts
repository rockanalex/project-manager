import { Component, OnInit, HostListener } from '@angular/core';
import { SprintsService } from 'src/app/services/sprints.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  innerWidth:any
  colWidth:any
  @HostListener('window:resize', ['$event'])
onResize(event) {
  this.innerWidth = window.innerWidth;
  if(this.innerWidth<640){
    this.colWidth = this.innerWidth
  }else{
    if(this.innerWidth>639&&this.innerWidth<960){
      this.colWidth = this.innerWidth/2
    }else{
      if(this.innerWidth>1280){
        this.colWidth = this.innerWidth/3
      }
    }
  }
  console.log(this.innerWidth, this.colWidth)
}

status: any[] = [
  {},
  { status: "1-ToStart", colorStatus: "ToStart", colorNumber: 1, c: "gris2", bg: "bg-gris2 negro" },
  { status: "2-Pending", colorStatus: "Pending", colorNumber: 2, c: "morado", bg: "bg-morado blanco" },
  { status: "3-WOI", colorStatus: "WOI", colorNumber: 3, c: "amarillo", bg: "bg-amarillo negro" },
  { status: "4-Review", colorStatus: "Review", colorNumber: 4, c: "azul2", bg: "bg-azul2 blanco" },
  { status: "5-Testing", colorStatus: "Testing", colorNumber: 5, c: "azul1", bg: "bg-azul negro" },
  { status: "6-Desicion", colorStatus: "Desicion", colorNumber: 6, c: "naranja", bg: "bg-naranja negro" },
  { status: "7-Completed", colorStatus: "Completed", colorNumber: 7, c: "verde", bg: "bg-verde blanco" },
  { status: "8-Stucked", colorStatus: "Stucked", colorNumber: 8, c: "rojo", bg: "bg-rojo blanco" },
  { status: "9-Archived", colorStatus: "Archived", colorNumber: 9, c: "gris", bg: "bg-gris blanco" },
  { status: "10-Operation", colorStatus: "Operation", colorNumber: 10, c: "naranja", bg: "bg-naranja blanco" }
]
actividades:any[]
bitacora:any[]
  users:any[]
  config:any
  user:any
  ss: SprintsService
  cookie: CookieService
  constructor(ss: SprintsService, cookie: CookieService) {
    this.ss = ss;
    this.cookie = cookie;
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
  if(this.innerWidth<640){
    this.colWidth = this.innerWidth
  }else{
    if(this.innerWidth>639&&this.innerWidth<1281){
      this.colWidth = this.innerWidth/2
    }else{
      if(this.innerWidth>1280){
        this.colWidth = this.innerWidth/3
      }
    }
  }
  console.log(this.innerWidth, this.colWidth)
    
    this.user = JSON.parse(this.cookie.get("usuario2"))
    this.config={
      server: "http://localhost:8081",
      usuario: this.user.wikiname,
      origen: this.user.origen
    }
    this.ss.setconfig(this.config)
    this.ss.getUsers(this.user.origen).subscribe(
      users => {
        this.users = users.detail
        this.actividades = [{"sub_eje":{"id":"20879","titulo":"Chat m\u00c3\u00b3vil (tipo Whatsapp)","descripcion":"Ir probando el chat individual tipo Whatsapp","topico":"","estatus":"2-Pending","colorNumber":"2","colorStatus":"Pending","active":"Active","avance":"0","prioridad":"High","prioridadNumber":"3","fecha_inicio":"2019-03-06","fecha_fin":"2019-03-06","fecha_real":"2019-03-06","metrica":"","responsable_1":"JoseRojas","responsable_2":"","encargado_ahora":"","padre":"17734","padre_titulo":"Genniux Apps","eje":"Ticket","link":"https:\/\/docs.google.com\/spreadsheets\/d\/1io7ToE-uolStl_loGy85HnYmyBX9I6HA2FvWuPFEgTM\/edit#gid=0","estimacion":"","timing":"TW","next_step":"","cuenta":"37","nCom":"4","nSpec":"0"}},{"sub_eje":{"id":"20309","titulo":"SUMATHON PPP","descripcion":"Necesitamos hacer un SUMATHON PPP (en Genniux)","topico":"","estatus":"1-ToStart","colorNumber":"1","colorStatus":"ToStart","active":"","avance":"0","prioridad":"Urgent","prioridadNumber":"2","fecha_inicio":"2019-01-02","fecha_fin":"2018-12-31","fecha_real":"0000-00-00","metrica":"","responsable_1":"","responsable_2":"JoseRojas","encargado_ahora":"","padre":"17734","padre_titulo":"Genniux Apps","eje":"Ticket","link":"","estimacion":"","timing":"TW","next_step":"","cuenta":"37","nCom":"0","nSpec":"4"}}]
        this.bitacora = [{"wikiname":"AngelesBravo","puesto":"34","reportes":[[{"id_bitacora":"526","id_actividad":"22426","titulo_actividad":"ELabs Website EmpowerLabs","id_reporter":"34","reporter":"AngelesBravo","descripcion":"Crear versi\u00f3n en ingles","cantidad":"2","unidad":"Horas","fecha":"2019-08-13","hora":"09:19:16","status":null}]]},{"wikiname":"PalomaMorales","puesto":"454","reportes":[[{"id_bitacora":"527","id_actividad":"22559","titulo_actividad":"ELabsCF Template para Presentaciones","id_reporter":"454","reporter":"PalomaMorales","descripcion":"Generar templates para portadas de presentaciones","cantidad":"2","unidad":"Horas","fecha":"2019-08-13","hora":"11:24:55","status":null}],[{"id_bitacora":"528","id_actividad":"22571","titulo_actividad":"ELabsCF - Templetes para Postings","id_reporter":"454","reporter":"PalomaMorales","descripcion":"Generar templates para postings en redes sociales","cantidad":"2","unidad":"Horas","fecha":"2019-08-13","hora":"11:27:01","status":null}]]},{"wikiname":"AbrahamRojas","puesto":"148","reportes":[[{"id_bitacora":"529","id_actividad":"22490","titulo_actividad":"Genniux Connect App","id_reporter":"148","reporter":"AbrahamRojas","descripcion":"Detalles de interfaz","cantidad":"0.5","unidad":"Horas","fecha":"2019-08-13","hora":"13:18:53","status":null},{"id_bitacora":"530","id_actividad":"22490","titulo_actividad":"Genniux Connect App","id_reporter":"148","reporter":"AbrahamRojas","descripcion":"Filtro de Notificaciones","cantidad":"0.5","unidad":"Horas","fecha":"2019-08-13","hora":"13:19:06","status":null},{"id_bitacora":"531","id_actividad":"22490","titulo_actividad":"Genniux Connect App","id_reporter":"148","reporter":"AbrahamRojas","descripcion":"Contador de Notificaciones","cantidad":"1","unidad":"Horas","fecha":"2019-08-13","hora":"13:19:39","status":null},{"id_bitacora":"532","id_actividad":"22490","titulo_actividad":"Genniux Connect App","id_reporter":"148","reporter":"AbrahamRojas","descripcion":"Marcar como visto una notificacion","cantidad":"0.5","unidad":"Horas","fecha":"2019-08-13","hora":"13:19:48","status":null},{"id_bitacora":"533","id_actividad":"22490","titulo_actividad":"Genniux Connect App","id_reporter":"148","reporter":"AbrahamRojas","descripcion":"Subir al servidor","cantidad":"0.5","unidad":"Horas","fecha":"2019-08-13","hora":"13:20:28","status":null}]]},{"wikiname":"JuanCarlosAngeles","puesto":"14","reportes":[[{"id_bitacora":"534","id_actividad":"22257","titulo_actividad":"Intelifin","id_reporter":"14","reporter":"JuanCarlosAngeles","descripcion":"Enviar factura a Roberto Rodriguez de Master Fleet SAPI de CV","cantidad":"0.1","unidad":"Horas","fecha":"2019-08-13","hora":"16:38:55","status":null},{"id_bitacora":"535","id_actividad":"22257","titulo_actividad":"Intelifin","id_reporter":"14","reporter":"JuanCarlosAngeles","descripcion":"Enviar factura a Armando Arturo Orozco por S\u00faper Pase Intelifin","cantidad":"0.1","unidad":"Horas","fecha":"2019-08-13","hora":"16:39:31","status":null},{"id_bitacora":"536","id_actividad":"22257","titulo_actividad":"Intelifin","id_reporter":"14","reporter":"JuanCarlosAngeles","descripcion":"Volver a solicitar datos de facturaci\u00f3n a Claudia Mata","cantidad":"0.1","unidad":"Horas","fecha":"2019-08-13","hora":"16:39:49","status":null}]]}]
      })
  }

  getUserImage(wikiname){
    let ret = {found:false,picture:""}
    this.users.forEach(element => {
      if(element.wikiname==wikiname){
        ret.found = true;
        ret.picture=element.picture
        return ret
      }
    });
    return ret
  }

}
