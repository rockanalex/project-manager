import { Component, OnInit, HostListener } from '@angular/core';
import { SprintsService } from 'src/app/services/sprints.service';
import { CookieService } from 'ngx-cookie-service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  @HostListener("scroll", ['$event'])
  scrollTabla($event: Event) {
    let scrollOffset = $event.srcElement.scrollTop;
    let scrollOffset2 = $event.srcElement.scrollLeft;
    let scroll2 = document.getElementById("scroll2");
    let scroll3 = document.getElementById("scroll3");
    scroll2.scrollTop = scrollOffset;
    scroll3.scrollLeft = scrollOffset2;
  }
  innerWidth:any
  colWidth:any
  @HostListener('window:resize', ['$event'])
onResize(event) {
  this.innerWidth = window.innerWidth;
  if(this.innerWidth<640){
    this.colWidth = this.innerWidth
  }else{
    if(this.innerWidth>639&&this.innerWidth<960){
      this.colWidth = this.innerWidth/2
    }else{
      if(this.innerWidth>1280){
        this.colWidth = this.innerWidth/3
      }
    }
  }
  console.log(this.innerWidth, this.colWidth)
}
  tabla: any;
  busqueda: string = ""
  current: any = {
    sprint: 0,
    toggleIzquierdo:false
  }
  newSpec : string = ""
  config:any
  selectedCel = -1;
  fecha1: Date = new Date()
  fecha2: Date = new Date()
  dias: any[] = []
  actividades: any[]
  sprints: any[] 
  toggleDerecho:Boolean=false;
  tabs:any = [
    { label: "Detalles", icon: "ion-ios-information-circle",index:0 },
    { label: "Specs", icon: "ion-ios-checkmark-circle",index:1 },
    { label: "Comentarios", icon: "ion-ios-chatboxes",index:2 },
    { label: "Recursos", icon: "ion-ios-play",index:3 },
    { label: "Archivos", icon: "ion-ios-copy",index:4 },
    { label: "Bitácora", icon: "ion-ios-timer",index:5 }
  ];
  status: any[] = [
    {},
    { status: "1-ToStart", colorStatus: "ToStart", colorNumber: 1, c: "gris2", bg: "bg-gris2 negro" },
    { status: "2-Pending", colorStatus: "Pending", colorNumber: 2, c: "morado", bg: "bg-morado blanco" },
    { status: "3-WOI", colorStatus: "WOI", colorNumber: 3, c: "amarillo", bg: "bg-amarillo negro" },
    { status: "4-Review", colorStatus: "Review", colorNumber: 4, c: "azul2", bg: "bg-azul2 blanco" },
    { status: "5-Testing", colorStatus: "Testing", colorNumber: 5, c: "azul1", bg: "bg-azul negro" },
    { status: "6-Desicion", colorStatus: "Desicion", colorNumber: 6, c: "naranja", bg: "bg-naranja negro" },
    { status: "7-Completed", colorStatus: "Completed", colorNumber: 7, c: "verde", bg: "bg-verde blanco" },
    { status: "8-Stucked", colorStatus: "Stucked", colorNumber: 8, c: "rojo", bg: "bg-rojo blanco" },
    { status: "9-Archived", colorStatus: "Archived", colorNumber: 9, c: "gris", bg: "bg-gris blanco" },
    { status: "10-Operation", colorStatus: "Operation", colorNumber: 10, c: "naranja", bg: "bg-naranja blanco" }
  ]
  cellWidth: number = 56
  nuevoProyecto: Proyecto = new Proyecto()
  nuevoSeparador: any = { id: Math.floor(Math.random() * 1000000), titulo: "", actividades: [] }
  proyectos: any[] = new Array()

  almacenamientoLocal: any
  user: any
  ss: SprintsService
  cookie: CookieService
  constructor(ss: SprintsService, cookie: CookieService) {
    this.ss = ss;
    this.cookie = cookie;
    this.almacenamientoLocal = window.localStorage
  }
  celda: any
  nodo: String = ""
  users: any
  ngOnInit() {
    setTimeout(() => {
      this.celda = document.getElementById("diaH-31");
      console.log(this.celda)
      //this.celda.scrollIntoView({  block: 'end', inline: 'start' })
      this.tabla = document.getElementById("tabla")
      let scroll3 = document.getElementById("scroll3")
      console.log(scroll3.scrollLeft)
      this.tabla.scrollLeft = scroll3.scrollLeft
    });
    console.log(this.cookie.get("usuario2"))
    this.user = JSON.parse(this.cookie.get("usuario2"))
    this.innerWidth = window.innerWidth;
  if(this.innerWidth<640){
    this.colWidth = this.innerWidth
  }else{
    if(this.innerWidth>639&&this.innerWidth<1281){
      this.colWidth = this.innerWidth/2
    }else{
      if(this.innerWidth>1280){
        this.colWidth = this.innerWidth/3
      }
    }
  }
  this.cellWidth = (this.innerWidth-((this.toggleDerecho?232:0)+300))/7
    if(this.user!==""&&this.user!==undefined&&this.user!==null){
      /*this.config={
        server: "../..",
        usuario: this.user.wikiname,
        origen: this.user.origen
      }*/
      this.config={
        server: "http://localhost:8081",
        usuario: this.user.wikiname,
        origen: this.user.origen
      }
      this.ss.setconfig(this.config)
      this.ss.getUsers(this.user.origen).subscribe(
        users => {
          console.log(this.user)
          console.log(users)
          this.users = users.detail
          this.ss.getProjects(this.user.origen).subscribe(
            projects => {
              console.log(projects)
              this.proyectos = projects.detail
              var paramsObject: any = {};
              window.location.search.replace(/\?/, '').split('&').map(function (o) { paramsObject[o.split('=')[0]] = o.split('=')[1] });
              this.nodo = paramsObject.proyecto;
              //alert(this.nodo)
              if (this.nodo !== "" && this.nodo !== undefined && this.nodo !== null) {
                var exists = false
                this.proyectos.forEach(element => {
                  if (element.id == this.nodo) {
                    exists = true;
  
                  }
                })
                if (exists) {
                  this.ss.getSprints(this.nodo).subscribe(
                    result => {
                      console.log(result)
                      this.sprints = result.detail
                      if (this.sprints.length > 0) {
                        this.current.sprint = this.sprints[0]
                        this.getSprintActivities(this.sprints[0].id)
                        this.setDate(this.current.sprint.fecha_inicio)
                      }else{
                        this.addSprint()
                        location.reload()
                      }
  
                    }
                  )
                } else {
                  //location.href = "./not-found"
                }
              } else {
                //location.href = "./not-found"
              }
            }
          )
        }
      )
    }else{
      location.href="../auth"
    }

  }

  getSprintActivities(id) {
    this.ss.getActivity('sprint_separador', id).subscribe(
      result => {
        console.log(result)
        this.actividades = result.detail
      }
    )
  }

  getElem(a, d) {
    return document.getElementById('item-' + a.id + '-' + d)
  }

  zoom(sentido) {
    switch (sentido) {
      case true:
        this.cellWidth < 56 ? this.cellWidth += 7 : this.cellWidth = 56
        break;
      case false:
        this.cellWidth > 28 ? this.cellWidth = (this.cellWidth - 7) : this.cellWidth = 28
        break;
    }
  }

  changeProject(proy) {
    window.open('./projects?proyecto=' + proy)
  }

  setDate(date) {
    var curr = new Date(date); // get current date
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first + 7; // last day is the first day + 6

    var firstday = new Date(curr.setDate(first));
    var lastday = new Date(curr.setDate(last));
    console.log(lastday)
    this.current.sprint.fecha_fin=lastday
    console.log(this.current.sprint.fecha_fin)
    this.updateSprint('fecha_fin',lastday,'id',this.current.sprint.id)
    this.dias=[]
    for(var i=0;i<7;i++){
      var tmp = new Date(curr.setDate(first+i))
      this.dias.push({
        dia:("0"+(tmp.getDate()+1)).slice(-2),
        mes: tmp.getMonth(),
        anio: tmp.getFullYear()
      })
    }
  }

  prepareProyecto() {
    this.nuevoProyecto = new Proyecto()
  }

  addProyecto() {
    this.nuevoProyecto.id = Math.floor(Math.random() * 1000000)
    this.nuevoProyecto.fecha_de_creacion = new Date()
    this.proyectos.push(this.nuevoProyecto)
  }

  addSeparador() {
    this.ss.addActivity(this.user.origen, 'sprint_separador', this.current.sprint.id, this.user.puesto, this.nuevoSeparador.titulo).subscribe(
      result => {
        console.log(result)
        this.nuevoSeparador.id = result.detail
        this.actividades.push(this.nuevoSeparador)
        this.nuevoSeparador = { id: Math.floor(Math.random() * 1000000), titulo: "", actividades: [] }
        //this.save()
      }
    )
  }

  addActividad(separador) {

    this.ss.addActivity(this.user.origen, 'sprint_activity', separador.id, this.user.puesto, "Nueva actividad").subscribe(
      result => {
        console.log(result)
        separador.actividades.push({
          titulo: "Nueva actividad",
          id: result.detail,
          bg: "bg-gris2 negro",
          dia: 1
        })
        //this.save()
      }
    )
    //this.save()
  }

  onPreHide($event) {
    this.save()
  }

  selected2:any

  selected: any = {
    tag: 0,
    element: {}
  }

  onRightClick($event, element, tag) {
    this.selected.tag = tag
    this.selected.element = element
    return false;
  }

  remove() {
    switch (this.selected.tag) {
      case 0:
        this.actividades.forEach((element, index) => {
          if (element.id == this.selected.element.id && element.actividades.length < 1) {
            this.actividades.splice(index, 1)
            this.delete(element)
          }
        });
        break;
      case 1:
        this.actividades.forEach((element, indice) => {
          if (element.id == this.selected.element.padre) {
            element.actividades.forEach((actividad, index) => {
              if (actividad.id == this.selected.element.id) {
                element.actividades.splice(index, 1)
                this.delete(actividad)
              }
            });
          }
        });
        break;
    }
  }

  addSprint() {
    this.ss.addSprint(this.nodo, "Sprint #" + this.sprints.length).subscribe(result => {
      this.ss.getSprints(this.nodo).subscribe(
        result => {
          this.sprints = result.detail
        }
      )
    })
  }

  update(c, v, co, vo) {
    this.ss.update(c, v, co, vo).subscribe(
      result => {
        console.log(result)
      }
    )
  }

  updateSprint(c, v, co, vo) {
    this.ss.updateSprint(c, v, co, vo).subscribe(
      result => {
        console.log(result)
      }
    )
  }

  updateResponsable(act, us, tag) {
    let u: any
    this.users.forEach(element => {
      if (element.wikiname == us) {
        u = element
      }
    });
    switch (tag) {
      case 2:

        this.update("responsable_2", u.puesto, "id", act.id)
        break;
    }
  }

  delete(nodo) {
    this.ss.delete(nodo.id).subscribe(
      result => {

      }
    )
  }

  save() {
    this.almacenamientoLocal.setItem("proyectos", JSON.stringify(this.actividades))
  }

  getUserImage(wikiname){
    let ret = {found:false,picture:""}
    this.users.forEach(element => {
      if(element.wikiname==wikiname){
        ret.found = true;
        ret.picture=element.picture
      }
    });
    return ret
  }

  cargarPanelDerecho(act){
    
    this.ss.getSpecs(this.user.wikiname,act.id).subscribe(
      specs => {
        console.log(specs)
        let res:any = specs
        if (res.status == "OK") {
          act.specs = res.detail;
      } else {
          act.specs = [];
      }
      this.selected2=act; this.toggleDerecho=true;
      }
    )
  }

  addSpec(){
    var date = new Date()
    this.ss.addSpecs(this.selected2.id,this.newSpec,this.user.puesto,{
      hour: ("0"+date.getMinutes()).slice(-2)+":"+("0"+date.getSeconds()).slice(-2),
      date: date.getFullYear()+"-"+("0"+(date.getMonth())).slice(-2)+"-"+("0"+date.getDate()).slice(-2)
    }).subscribe(result=>{
      console.log(result)
      this.newSpec = ""
    })
  }

  drop(event: CdkDragDrop<any[]>) {
    console.log(event)
    moveItemInArray(this.sprints, event.previousIndex, event.currentIndex);
  }

}

class Proyecto {
  nombre: string
  id: number
  creador: string
  fecha_de_creacion: Date
  status: string

  constructor() {
    this.nombre = "Nuevo proyecto"
    this.status = "Pending"
  }
}
