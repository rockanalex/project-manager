import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  busqueda:string=""
  options:any[]

  constructor() { }

  ngOnInit() {
    this.options = [
      {
        text:"Usuario",
        action: function(){}
      },
      {
        text:"Agenda",
        action: function(){}
      },
      {
        text:"Ajustes",
        action: function(){}
      },
      {
        text:"Salir",
        action: function(){}
      }
    ]
  }

}
