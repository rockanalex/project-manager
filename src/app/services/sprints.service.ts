import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, config } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SprintsService {

  config:any
  url:string;

  constructor(public http: HttpClient
    ) { }

    setconfig(c){
      this.config=c
    }

    getUsers(cuenta):Observable<any>{
      return this.http.get(this.config.server+"/php/group/getUsersArray.php?origin="+cuenta);
    }

    getProjects(cuenta):Observable<any>{
      return this.http.get(this.config.server+"/php/pap/sprints/getProjects.php?cuenta="+cuenta);
    }

getSprints(proyecto):Observable<any>{
  return this.http.get(this.config.server+"/php/pap/sprints/getSprints.php?proyecto="+proyecto);
}

getActivity(type,padre):Observable<any>{
  return this.http.get(this.config.server+"/php/pap/sprints/getActivity.php?padre="+padre+
  "&type="+type);
}

getSpecs(user,act_id){
  return this.http.get(this.config.server+"/php/specsAPI/consultaSpec.php?user=" + user + "&id=" + act_id)
}

addSpecs(id,descripcion,puesto,time){
  return this.http.get(this.config.server+"/php/specsAPI/altaSpec.php?objetivo=" + id 
  + "&descripcion=" + descripcion + "&colaborador=" 
  + puesto + "&fecha=" + time.date + "&hora=" + time.hour+"&medida=0")
}

addSprint(proyecto, titulo):Observable<any>{
  return this.http.post(this.config.server+"/php/pap/sprints/addSprint.php",{
    obj:{
      proyecto:proyecto,
      titulo: titulo
    }
  });
}

delete(id):Observable<any>{
  return this.http.get(this.config.server+"/php/pap/deleteCarefully.php?id="+id);
}

addActivity(cuenta,type,padre,responsable, titulo):Observable<any>{
  return this.http.post(this.config.server+"/php/pap/sprints/addActivity.php",{
    obj:{
      titulo: titulo,
      responsable:responsable,
      padre: padre,
      type:type,
      cuenta: cuenta
    }
  });
}

update(campo, valor, campoObjetivo, valorObjetivo):Observable<any>{
  return this.http.post(this.config.server+"/php2alex/pap/update.php",{
    tabla: "pap_sub_eje",
    campo: campo,
    valor: valor,
    campoObjetivo: campoObjetivo,
    valorObjetivo: valorObjetivo
  });
}

updateSprint(campo, valor, campoObjetivo, valorObjetivo):Observable<any>{
  return this.http.post(this.config.server+"/php2alex/pap/update.php",{
    tabla: "sprint",
    campo: campo,
    valor: valor,
    campoObjetivo: campoObjetivo,
    valorObjetivo: valorObjetivo
  });
}

}
